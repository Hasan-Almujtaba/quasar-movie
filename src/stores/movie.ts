import { defineStore } from 'pinia';
import { computed, ref } from 'vue';

export const useMovieStore = defineStore(
  'movie',
  () => {
    const keyword = ref('');

    const search = (userType: string) => {
      console.log(userType);
      keyword.value = userType;
    };

    const movies: any = ref([]);

    const filteredMovies = computed(() => {
      if (keyword.value === '') return movies.value;

      return movies.value.filter((item: { title: string }) =>
        item.title.toLowerCase().includes(keyword.value)
      );
    });

    const getMovie = (id: number) => {
      return movies.value.find(
        (item: { id: number }) => item.id === Number(id)
      );
    };

    const store = (movie: any) => {
      movies.value.push(movie);
    };

    const update = (movie: any) => {
      const index = movies.value.findIndex(
        (item: { id: any }) => item.id === movie.id
      );

      if (index !== -1) {
        movies.value[index] = movie;
      }
    };

    const remove = (movie: any) => {
      const index = movies.value.findIndex(
        (item: { id: number }) => item.id == movie.id
      );

      if (index !== -1) {
        movies.value.splice(index, 1);
      }
    };

    return {
      keyword,
      search,
      movies,
      filteredMovies,
      getMovie,
      store,
      update,
      remove,
    };
  },
  {
    persist: true,
  }
);
